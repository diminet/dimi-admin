<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Soho administration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<link href="css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="css/main.css" rel="stylesheet" media="screen">
	<link href="css/font-awesome.css" rel="stylesheet" media="screen">
	<link href="css/bootstrap-responsive.css" rel="stylesheet" media="screen">
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a href="#" class="brand">Administrations panel</a>
			<div class="navbar-text pull-right">
				Du er logget ind som:
				<div class="btn-group">
					<a class="btn btn-inverse" href="#"><i class="icon-user"></i>&nbsp;&nbsp; Arlind Ukshini</a>
					<a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-caret-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#"><i class="icon-pencil"></i> Ret oplysninger</a></li>
						<li><a href="#"><i class="icon-share-alt"></i> Se hjemmeside</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-signout"></i> Log ud</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="span3">
			<div class="well">
				<ul class="nav nav-list">
					<li><a href="#"><i class="icon-home"></i> Oversigt</a></li>
					<li class="divider"></li>
					<li class="nav-header">Rediger side tekst</li>
					<li><a href="#"><i class="icon-pencil"></i> Forside</a></li>
					<li><a href="#"><i class="icon-pencil"></i> Booking</a></li>
					<li><a href="#"><i class="icon-pencil"></i> Events</a></li>
					<li><a href="#"><i class="icon-pencil"></i> Politik</a></li>
					<li class="nav-header">Funktioner</li>
					<li class="disabled"><a href="#"><i class="icon-star"></i> Begivenheder</a></li>
					<li class="disabled">
						<a href="#"><i class="icon-bookmark"></i> Resvervationer
						<span class="pull-right notification">1</span></a>
					</li>
					<li><a href="#"><i class="icon-th"></i> Events</a></li>
					<li><a href="#"><i class="icon-camera-retro"></i> Galleri</a></li>
					<li><a href="#"><i class="icon-group"></i> Brugere</a></li>
					<li class="nav-header">Oplysninger</li>
					<li><a href="#"><i class="icon-phone"></i> Kontakt info</a></li>
					<li><a href="#"><i class="icon-time"></i> Åbningstider</a></li>
				</ul>
			</div>
		</div> <!-- /span -->
		<div class="span9">
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
			<h1>Hello, world!</h1>
		</div> <!-- /span -->
	</div> <!-- /row -->
</div> <!-- /container -->

<script src="http://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.js"></script>

</body>
</html>